package listen;

import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import utils.File;
import utils.HttpGET;
import utils.Mail;
import utils.Network;

public class Main {
	private static volatile boolean overdueStatus;//  上次的状态
	private static volatile boolean dayStatus = true;//  每天发送的状态
	private static volatile String ip;// 外网ip
	private static long min = 2 * 60 * 1000;// 2分钟检查一次
	private static long day = 8;// 每天早上8点定时发送一次
	private static long perform;// 计数器上限
	private static long index;// 线程执行次数
	private static int timeOut = 3000;// 默认web测试超时时间 3秒
	private static final String url = "http://test.com:81/TKTestLogin.json?userId=1";// web服务器地址
	private static final Set<String> hosts = new HashSet<>();// 关联的host列表
	private static final String hosts_url = "/etc/hosts";// 监测服务器host文件地址
	private static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	static {
		ip = Network.getIpHost();
		// 初始化计数器
		dayCalculate();
		hosts.add("test.com");
		hosts.add("happyinteract.ews.m.jaeapp.com");
	}

	static class Tesk implements Runnable {
		@Override
		public void run() {
			// TODO Auto-generated method stub
			try {
				while (true) {
					if (index == perform) {
						dayStatus = true;
						// 重置计数器
						index = 0;
						perform = 24 * 60 / 2;
					}
					listenNetwork(overdueStatus);
					Thread.sleep(min);
					index++;
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private static void listenNetwork(boolean _overdueStatus) {
		boolean status = false;
		boolean _status = false;
		String data = format.format(new Date());
		try {
			if (null == ip) {
				ip = Network.getIpHost();
			}
			if (null != ip) {
				status = InetAddress.getByName(ip).isReachable(timeOut);
				if (status) {
					// 模拟web请求确定ip可用
					try {
						String html = HttpGET.sendHttpGet(url, "utf-8");
						_status = true;
						if (null == html) {
							_status = false;
						}
					} catch (Exception e) {
						// TODO: handle exception
						_status = false;
					}
					status = _status;
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		}
		overdueStatus = status;
		String title = "乌班图（192.168.0.25）网络情况报告", content = "请检查网络监听程序";
		if (status) {
			// 网络正常
			content = "网络状态:正常,外网ip为[" + ip + "] " + Network.getIpTerritorial();
			if (!_overdueStatus) {
				// 上次调用不通，但是这次通了 需要调用脚本更新监测机器host ip映射
				File.writer(File.reader(url, hosts, ip), url);
			}
		} else {
			// 外网不通
			if (null == ip) {
				// 从未连接到外网
				content = "网络状态:从未连接到外网,无法获得外网ip地址,请检查路由";
			} else if (!_status) {
				// 网络不通
				content = "网络状态:使用数据网络测试连接: " + url + " 失败";
			}
		}
		content = "[" + data + "] " + content;
		// 是因为上次网络状态不通或每天一次的状态报告才发送
		if (_overdueStatus != status || dayStatus) {
			System.out.println("开始发送邮件" + content);
			boolean fag=sendMail(title, content);
			if(fag){
				if (dayStatus) {
					dayStatus = false;
				}
			}
		} else {
			System.out.println("【不发送】" + content);
		}
	}

	/**
	 * 发送邮件
	 */
	private static boolean sendMail(String title, String content) {
		Set<String> receiveMails = new HashSet<>();
		receiveMails.add("790809568@qq.com");
		receiveMails.add("1289643364@qq.com");
		receiveMails.add("776489424@qq.com");
		receiveMails.add("370256646@qq.com");
		receiveMails.add("392643256@qq.com");
		String sendName = "Ubuntu";
		boolean fag=false;
		try {
			fag=Mail.mailClient(receiveMails, sendName, title, content);
			System.out.println("邮件发送情况："+fag);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return fag;
	}

	private static void dayCalculate() {
		String data = format.format(new Date());
		// 获得当前的小时数
		int hours = Integer.valueOf(data.substring(11, 13));
		// 下次执行时间差
		long su = hours - day;
		if (su > 0) {
			su = 24 - su;
		} else {
			su = Math.abs(su);
		}
		perform = su * 60 / 2;
	}

	public static void main(String[] args) {
		new Thread(new Tesk()).start();
	}
}
