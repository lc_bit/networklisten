package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
/*********
 * 进行httppost请求  回来的都是字符串   不能拿来请求文件下载
 * @author Administrator
 *
 */
public class HttpPOST {
	  public static String  sendHttpPost(String url,String parameterData,String coding) throws IOException{
		    URL localURL = new URL(url);
		    URLConnection connection = localURL.openConnection();
		    HttpURLConnection httpURLConnection = (HttpURLConnection)connection;
		    
		    httpURLConnection.setDoOutput(true);
		    httpURLConnection.setRequestMethod("POST");
		    httpURLConnection.setRequestProperty("Accept-Charset", "utf-8");
		    httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		    httpURLConnection.setRequestProperty("Content-Length", String.valueOf(parameterData.length()));
		    
		    OutputStream outputStream = null;
		    OutputStreamWriter outputStreamWriter = null;
		    InputStream inputStream = null;
		    InputStreamReader inputStreamReader = null;
		    BufferedReader reader = null;
		    StringBuffer resultBuffer = new StringBuffer();
		    String tempLine = null;
		    
		    try {
		        outputStream = httpURLConnection.getOutputStream();
		        outputStreamWriter = new OutputStreamWriter(outputStream);
		        
		        outputStreamWriter.write(parameterData.toString());
		        outputStreamWriter.flush();
		        
		        if (httpURLConnection.getResponseCode() >= 300) {
		            try {
						throw new Exception("HTTP Request is not success, Response code is " + httpURLConnection.getResponseCode());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		        }
		        
		        inputStream = httpURLConnection.getInputStream();
		        inputStreamReader = new InputStreamReader(inputStream, null==coding?"utf-8":coding);
		        reader = new BufferedReader(inputStreamReader);
		        
		        while ((tempLine = reader.readLine()) != null) {
		            resultBuffer.append(tempLine);
		        }
		        
		    } finally {
		        
		        if (outputStreamWriter != null) {
		            outputStreamWriter.close();
		        }
		        
		        if (outputStream != null) {
		            outputStream.close();
		        }
		        
		        if (reader != null) {
		            reader.close();
		        }
		        
		        if (inputStreamReader != null) {
		            inputStreamReader.close();
		        }
		        
		        if (inputStream != null) {
		            inputStream.close();
		        }
		        
		    }

		    return resultBuffer.toString();
	  }
}
