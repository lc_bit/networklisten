package utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class File {
	/**
	 * 读取
	 * @param url
	 * @param hosts
	 * @param ip
	 * @return
	 */
	public static List<String> reader(String url, Set<String> hosts, String ip) {
		List<String> list = new ArrayList<>();
		try {
			// 读
			FileReader reader = new FileReader(url);
			BufferedReader br = new BufferedReader(reader);
			String str = null;
			while ((str = br.readLine()) != null) {
				boolean fag = true;
				if (null != hosts) {
					for (String host : hosts) {
						if (str.contains(host)) {
							fag = false;
							list.add(ip + "  " + host);
							break;
						}
					}
				}
				if (fag) {
					list.add(str);
				}
			}
			br.close();
			reader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;

	}
    /**
     * 写入
     * @param list
     * @param url
     */
	public static void writer(List<String> list, String url) {
		try {
			// 写
			FileWriter writer = new FileWriter(url);
			BufferedWriter bw = new BufferedWriter(writer);
			for (String s : list) {
				bw.write(s);
				bw.newLine();// 换行
			}
			bw.flush();
			bw.close();
			writer.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public static void main(String[] args) {
		Set<String> hosts = new HashSet<>();// 关联的host列表
		hosts.add("test.com");
		hosts.add("happyinteract.ews.m.jaeapp.com");
		String url = "/Users/ytx/Documents/192.168.0.25/host";
		writer(reader(url, hosts, "192.168.0.1"), url);
		System.out.println("ok");
	}
}
