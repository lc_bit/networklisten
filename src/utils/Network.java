package utils;

public class Network {
	private static String ip;
	private static String host;
	private static String territorial;
	static{
		getIp();
	}
	/**
	 * 获得外网ip地址
	 * @return
	 */
	public static String getIp() {
		try {
			String html = HttpGET.sendHttpGet("http://1212.ip138.com/ic.asp", "gbk");
			String startLabel = "<center>";
			String endLabel = "</center>";
			ip = html.substring(html.indexOf(startLabel), html.indexOf(endLabel));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return getIpHost();
	}
	/**
	 * 获得外网ip地址 HOST
	 * @return
	 */
	public static String getIpHost() {
		host=null!=ip?(ip.substring(ip.indexOf("[")+1, ip.indexOf("]"))):null;
		return host;
	}
	/**
	 * 获得外网ip归属地地址 territorial
	 * @return
	 */
	public static String getIpTerritorial() {
		territorial=null!=ip?(ip.substring(ip.indexOf("]")+1, ip.length())):null;
		return territorial;
	}
	public static void main(String[] args) {
		String str=getIp();
		System.out.println(str=str.substring(str.indexOf("[")+1, str.indexOf("]")));
	}
}
